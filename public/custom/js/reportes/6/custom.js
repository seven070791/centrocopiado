$('.select2').select2({
    theme:"bootstrapbs4",
});
//Evento para resetear el formulario
$("button[type=reset]").on('click', function(){
    $("#detalles").hide();
    $("#consulta-fecha")[0].reset();
});

//Evento para ocultar los proveedores para la salida
$("#tipo-consulta").on('change', function(){
    if($("#tipo-consulta").val() == "2"){
        $("#proveedor-consulta").attr('disabled', "true");
    }
    else{
        $("#proveedor-consulta").removeAttr('disabled');   
    }
});

//Evento para la consulta de reportes de entradas y salidas por rango de fecha
$("#rango-consulta").on('focus', function(e){
	e.preventDefault();
	$("#rango-consulta").daterangepicker({
		showDropdowns: true,
        maxDate: fecha(),
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
	});
});

/*Funcion que realiza la fecha de hoy*/
function fecha(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yy = hoy.getFullYear();
    var fecha = '';
    if(dd<10){
        dd = '0'+dd;
    }
    else if(mm<10){
        mm = '0'+mm;
    }
    fecha = dd+"/"+mm+"/"+yy;
    return fecha;
}
//Funcion que invierte la fecha para la BD
function invertirFecha(fecha){
	let fectmp = fecha.split('/');
	let fechadb = `${fectmp[2]}-${fectmp[1]}-${fectmp[0]}`;
	return fechadb;
}

//Evento para el envio de la consulta por fecha
$("#consulta-fecha").on('submit', function(e){
	e.preventDefault();
	let fechas = $("#rango-consulta").val().split("-");
	let datos = {
        'modo'              : $("#tipo-consulta").val(),
		'fecha_inicio'      : invertirFecha(fechas[0].trim()),
		"fecha_fin"         : invertirFecha(fechas[1].trim()),
        'usuario-consulta'  : $("#usuario-consulta").val(),
        'producto-consulta' : $("#producto-consulta").val(),
        'proveedor-consulta': $("#proveedor-consulta").val(),
	}
	$.ajax({
		url:"/consultaFecha",
		method:"POST",
		dataType:"JSON",
		data: {
			"data" : btoa(JSON.stringify(datos))
		},
		beforeSend: function(){
			$("button[type=submit]").attr("disabled", "true");
		}
	}).then(function(response){
		$("#detalles-consulta").html(atob(response.data.tabla));
        $("#generar-reporte").html(response.data.footer);
		$("#detalles").show();
	}).catch(function(request){
		Swal.fire("Atencion", request.message, "warning");
	});
	$("button[type=submit]").removeAttr("disabled");
});


//Evento para generar el reporte en PDF
$(document).on('click',"#genera-pdf" ,function(e){
    e.preventDefault();
    if($("#rango-consulta").val() == ''){
        Swal.fire("Atencion", "Debe escoger una fecha", "warning");
    }
    else{
        let fechas = $("#rango-consulta").val().split("-");
        let datos = {
            'modo'              : $("#tipo-consulta").val(),
            'fecha_inicio'      : invertirFecha(fechas[0].trim()),
            "fecha_fin"         : invertirFecha(fechas[1].trim()),
            'usuario-consulta'  : $("#usuario-consulta").val(),
            'producto-consulta' : $("#producto-consulta").val(),
            'proveedor-consulta': $("#proveedor-consulta").val(),
        }
        let query = btoa(JSON.stringify(datos));
        window.location = "/generareporte/6?q="+query;    
    }
});

$(document).ready(function(){
    $("#detalles").hide();
});


