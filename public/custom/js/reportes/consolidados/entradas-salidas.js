$(document).ready(function(){
	$("#grafico").hide();
})

//Inicializando calendario
$("#rango-consulta").on('focus', function(e) {
	e.preventDefault();
	$("#rango-consulta").daterangepicker({
		showDropdowns: true,
		maxDate: fecha(),
		locale: {
			format: 'DD/MM/YYYY',
			daysOfWeek: [
				"Do",
				"Lu",
				"Ma",
				"Mi",
				"Ju",
				"Vi",
				"Sa"
			],
			monthNames: [
				"Enero",
				"Febrero",
				"Marzo",
				"Abril",
				"Mayo",
				"Junio",
				"Julio",
				"Agosto",
				"Septiembre",
				"Octubre",
				"Noviembre",
				"Diciembre"
			]
		}
	});
});


//Evento de envio de formulario al controlador
$(document).on('submit', "#consulta-articulos", function(e) {
	e.preventDefault();
	let fechas = $("#rango-consulta").val().split(" - ");
	let datos = {
		"date_init" : invertirFecha(fechas[0]),
		"date_end"  : invertirFecha(fechas[1]),
		"mode"      : $("#tipo-operacion").val()
	}
	$.ajax({
		url    : "/obtenerConsolidado",
		method : "POST",
		data : {"data" : btoa(JSON.stringify(datos))},
		beforeSend: function(){
			$("button[type=submit]").attr('disabled');
		}
	}).then((response) => {
		$("#tabla").html(response.tabla);
		tabla('.tabla');
		$("#grafico").show();
		//Generamos el grafico aqui
		var ctx = document.getElementById('salida').getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: response.dataset.label,
				datasets: [{
					label: 'Nº de unidades',
					data: response.dataset.data,
					backgroundColor: '#003b2c'
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}).catch((request) => {
		Swal.alert("Error", request.messageJSON.message, "error");
	});
});

//Generacion de archivo csv 
$(document).on('click', "#generaArchivoExcel", function(e){
	e.preventDefault();
	let fechas = $("#rango-consulta").val().split(" - ");
	window.location = '/generarConsolidadoExcel/' + invertirFecha(fechas[0]) + '/' + invertirFecha(fechas[1]) + '/' + $("#tipo-operacion").val();
})