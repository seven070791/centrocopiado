<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta lang="es">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Gestión | Centro Copiado</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/logo_menu.png" style="width: 50px; height: 50px;">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/fontawesome-free/css/all.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/dist/css/adminlte.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/summernote/summernote-bs4.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/daterangepicker/daterangepicker.css">
  <!-- Ion Slider -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/select2/css/select2.min.css">
 <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
 <!-- DataTables -->
 <link rel="stylesheet" href="<?php echo base_url();?>/theme/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
</head>