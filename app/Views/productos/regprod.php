  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">

          <input type="checkbox" style="display: none;" class="borrado" id="borrado" name="borrado" value='false'>
          <input type="hidden" name="borrado_actual" id="borrado_actual" value="0">
          <h3 class="card-title">Registro de un nuevo producto</h3>
        </div>
        <form role="form" method="POST" id="nuevoproducto">
          <input type="hidden" id="modform" name="modform" value="1">
          <div class="card-body">
            <div class="form-group">
              <label for="codbar">Código de barras</label>
              <input class="form-control" type="number" name="codbar" id="codbar">
            </div>
            <div class="form-group">
              <label for="prodmar">Marca del producto</label>
              <input class="form-control" type="text" name="prodmar" id="prodmar">
            </div>
            <div class="form-group">
              <label for="prodmodel">Descripcion</label>
              <input class="form-control" type="text" name="prodmodel" id="prodmodel">
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a type="button" class="btn btn-danger" href="javascript:history.back()">Cerrar</a>
          </div>
          <!-- /.card-footer-->
        </form>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->