  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Editar Producto &nbsp;&nbsp;&nbsp;&nbsp;<label for="user-pass">Activo</label>&nbsp;&nbsp;
            <input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>
            <input type="hidden" name="borrado_actual" id="borrado_actual" value="<?php echo $borrado; ?>">
          </h3>

        </div>
        <form role="form" method="POST" id="nuevoproducto">
          <input type="hidden" id="modform" name="modform" value="<?php echo $modform; ?>">
          <div class="card-body">
            <div class="form-group">
              <label for="codbar">Código de barras</label>
              <input class="form-control" disabled type="number" name="codbar" id="codbar" value="<?php echo $codbar; ?>">
            </div>
            <div class="form-group">
              <label for="prodmar">Marca del producto</label>
              <input class="form-control" type="text" name="prodmar" id="prodmar" value="<?php echo $prodmar; ?>">
            </div>
            <div class="form-group">
              <label for="prodmodel">Descripcion</label>
              <input class="form-control" type="text" name="prodmodel" id="prodmodel" value="<?php echo $prodmodel; ?>">
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a type="button" class="btn btn-danger" href="javascript:history.back()">Cerrar</a>
          </div>
          <!-- /.card-footer-->
        </form>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->