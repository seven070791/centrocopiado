	<script type="text/javascript">
		function tabRequisiciones() {
			$.ajax({
				url: '/obtpreordenes',
				method: 'POST',
				success: function(response) {
					if (response && response.data) {
						var decodedResponse = decodeURIComponent(response.data);
						$("#requisiciones").html(decodedResponse);
					} else {
						console.error("La respuesta no contiene la propiedad 'data' válida.");
					}
				}
			});
		}

		function tabRequerimientos() {
			$.ajax({
				url: "/obtrequerimientos",
				method: "POST",
				success: function(response) {
					if (response && response.data) {
						var decodedResponse = decodeURIComponent(response.data);
						$("#requerimientos").html(decodedResponse);
					} else {
						console.error("La respuesta no contiene la propiedad 'data' válida.");
					}
				}
			});
		}

		$(document).ready(function() {
			tabRequisiciones();
			tabRequerimientos();
		});
	</script>
	</body>

	</html>