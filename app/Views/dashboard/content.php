<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tablero</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <!-- container-fluid -->
    <div class="container-fluid">
      <!-- /.row -->
      <div class="row">
        <section class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">
                Requisiciones Solicitadas
              </h3>
            </div>
            <div class="card-body">
              <table class="table table-light table-hover table-bordered text-center">
                <thead>
                  <tr>
                    <td>Nº Orden</td>
                    <td>Fecha de Solicitud</td>
                    <td>Estatus</td>
                    <td>Comentario</td>
                    <td>Detalles</td>
                  </tr>
                </thead>
                <tbody id="requisiciones">
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
      <!-- <div class="row">
        <section class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">
                Requerimientos Solicitados
              </h3>
            </div>
            <div class="card-body">
              <div id="requerimientos">
              </div>
            </div>
          </div>
        </section>
      </div> -->

    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>