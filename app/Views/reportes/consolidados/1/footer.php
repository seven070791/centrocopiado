	<!-- date-range-picker -->
 	<script src="<?php echo base_url();?>/theme/plugins/daterangepicker/daterangepicker.js"></script>
 	<!-- Select2 -->
 	<script src="<?php echo base_url();?>/theme/plugins/select2/js/select2.full.min.js"></script>
	<!-- OPTIONAL SCRIPTS -->
	<script src="<?php echo base_url();?>/theme/plugins/chart.js/Chart.min.js"></script>
	<!--Custom scripts-->
	<script type="text/javascript" src="<?php echo base_url();?>/custom/js/reportes/consolidados/entradas-salidas.js"></script>
</body>
</html>