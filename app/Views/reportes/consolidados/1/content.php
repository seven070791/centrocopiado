  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 p-2">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Consolidados de entradas y salidas de articulos</h3>
                    <button class="btn btn-light" id="generaArchivoExcel"><i class="fas fa-file-excel"></i> Generar Archivo Excel</button>
                </div>
              </div>
              <div class="card-body">
                <!--Form-->
                <form id="consulta-articulos" method="POST">
                  <div class="row">
                    <div class="col-5">
                      <label for="tipo-operacion">Rango de consulta</label>
                      <input type="text" class="form-control" id="rango-consulta" name="rango-consulta" placeholder="Haga clic para seleccionar rango de consulta" required>
                    </div>
                    <div class="col-5">
                      <label for="tipo-operacion">Tipo de operacion</label>
                      <select id="tipo-operacion" name="tipo-operacion" class="form-control">
                        <option value="1">Entradas</option>
                        <option value="2">Salidas</option>
                      </select>
                    </div>
                    <div class="col-2">
                      <div class="p-3"></div>
                      <button type="submit" class="btn btn-primary">Consultar</button>
                    </div>
                  </div>
                </form>
                <!--Chart-->
                <div class="row" id="grafico">
                  <div class="col-12 p-4">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg">Grafico de operaciones</span>
                      </p>
                    </div>
                    <!-- /.d-flex -->
                    <div class="position-relative mb-4">
                      <canvas id="salida" height="200"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /Chart-->
                <!--Table result-->
                <div class="row">
                  <div class="col-12 p-2">
                    <div id="tabla">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->