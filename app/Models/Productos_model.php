<?php

namespace App\Models;

class Productos_model extends BaseModel
{

	/*Metodo que registra nuevos productos*/
	public function newProd(array $data)
	{

		$builder = $this->dbconn('sta_productos');
		$query = $builder->insert($data);
		$query2 = $this->registrarEnExistencias($data['codbar']);
		return $query2;
	}

	/*Metodo que verifica (recursivamente) si un producto existe o no*/
	public function isProductExists(String $id)
	{
		$query = $this->getSingle($id);
		if ($query->resultID->num_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*Metodo que obtiene todos los productos*/

	public function getAllProd()
	{

		$builder = $this->dbconn('sta_productos as p');
		$builder->select(
			"p.codbar,p.prodmar,p.prodmodel,case when p.borrado=0 then'ACTIVO' else 'INACTIVO ' end as borrado"
		);
		//$builder->where(['p.borrado' => 0]);
		$query = $builder->get();
		return $query;
	}

	/*Metodo que permite obtener un solo producto*/

	public function getSingle($id)
	{
		$builder = $this->dbconn('sta_productos');
		$query = $builder->getWhere(array('codbar' => $id));
		return $query;
	}
	/*Metodo que actualiza la informacion registrada*/

	public function updateProd(array $data)
	{
		$builder = $this->dbconn('sta_productos');
		$builder->where('codbar', $data['codbar']);
		$query = $builder->update($data);
		return $query;
	}
}
